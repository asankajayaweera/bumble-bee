<%@ page import="com.DAO.cartDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.User" %>
<%@ page import="com.entity.cart" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/30/2023
  Time: 5:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
  <%@include file="all_component/all_css.jsp"%>
   <title>Cart Page</title>
</head>
<body style=" background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>
<c:if test="${empty userobj}">
  <c:redirect url="logging.jsp"></c:redirect>
</c:if>

<c:if test="${not empty succMsg}">
  <div class="alert alert-primary" role="alert">
    ${succMsg }
  </div>
  <c.remove var="succMsg" scope="session"/>
</c:if>

<c:if test="${not empty failedMsg}">
  <div class="alert alert-danger" role="alert">
    ${failedMsg }
  </div>
  <c.remove var="failedMsg" scope="session"/>
</c:if>


<div class="container">
  <div class="row p-3">
    <div class="col-md-7">
      <div class="card bg-white">
        <div class="card-body">
      <h3 class="text-center text-warning">Your Selected Devices...</h3>
      <table class="table table-striped">
        <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Modal</th>
          <th scope="col">Price</th>
          <th scope="col">Action</th>

        </tr>
        </thead>
        <tbody>
        <%
          User u=(User)session.getAttribute("userobj");

          cartDAOImpl dao=new cartDAOImpl(DBConnect.getConn());
          List<cart> cart=dao.getDeviceByUser(u.getId());
          Double totalPrice=0.00;
          for(cart c:cart){
            totalPrice=c.getTotalPrice();
            %>
        <tr>
          <th scope="row"><%=c.getBrandName()%></th>
          <td><%=c.getModalName()%></td>
          <td><%=c.getPrice()%></td>
          <td>
            <a href="remove_device?did=<%=c.getDid()%>" class="btn btn-danger">Remove</a>
          </td>
        </tr>
        <%
          }
        %>
        <tr>
          <td>Total Price</td>
          <td></td>
          <td></td>
          <td><%=totalPrice%></td>
        </tr>
        </tbody>
      </table>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
          <h3 class="text-center text-warning">Your Order Details</h3>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Name</label>
                <input type="text" class="form-control" id="inputEmail4" value="">
              </div>

              <div class="form-group col-md-6">
                <label for="inputPassword4">Email</label>
                <input type="email" class="form-control" id="inputEmail" >
              </div>

              <div class="form-group col-md-6">
                <label for="inputPassword4">Phone Number</label>
                <input type="email" class="form-control" id="inputPassword4" >
              </div>

              <div class="form-group">
                <label>Payment Method</label>
                <select class="form-control">
                <option>--Select--</option>
              <option>Cash On delivery</option>
                </select>
            </div>
              <div class="text-center">
                <button class="btn btn-success"><i class="fa-brands fa-first-order"></i> Order Now</button>
                <a href="index.jsp" class="btn btn-warning"><i class="fa-solid fa-cart-shopping"></i> Continue Shopping</a>
              </div>
          </form>
        </div>
      </div>

    </div>
  </div>

</div>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</body>
</html>
