<%@ page import="com.DAO.DeviceDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.DeviceDetail" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/29/2023
  Time: 8:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Laptops</title>
    <%@include file="all_component/all_css.jsp"%>
    <style type="text/css">
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }.card:hover .card-body {
             background-color: #FcF7F7;
         }
    </style>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>
<c:if test="${not empty addCart}">
    <div id="toast"> ${addCart} </div>

    <script type="text/javascript">
        showToast();
        function showToast(content)
        {
            $('#toast').addClass("display");
            $('#toast').html(content);
            setTimeout(()=>{
                $("#toast").removeClass("display");
            },2000)
        }
    </script>
</c:if>

<div class="container-fluid">
    <div class="row">
        <%
            DeviceDAOImpl dao3=new DeviceDAOImpl(DBConnect.getConn());
            List<DeviceDetail> list3=dao3.getAllLaptop();
            for (DeviceDetail d:list3){
        %>
        <div class="col-md-3">
            <div class="card crd-ho mt-2">
                <div class="card-body text-center">
                    <img alt="" src="device/<%=d.getPhotoname()%>" style="width:200px; height:200px" class="img-thumblin">
                    <h6><%=d.getBrandname()%></h6>
                    <div class="row">
                        <a href="" class="btn btn-danger btn-sm ml-3">Add to Cart</a>
                        <a href="view_Device.jsp?did=<%=d.getDeviceid()%>" class="btn btn-primary btn-sm ml-1">View</a>
                        <a href="" class="btn btn-dark btn-sm ml-1">Rs.<%=d.getPrice()%></a>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>

</div>

</div>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</body>
</html>
