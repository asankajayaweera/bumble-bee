<%@ page import="com.entity.DeviceDetail" %>
<%@ page import="java.util.List" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.DAO.DeviceDAOImpl" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/29/2023
  Time: 8:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>All Mobiles Phones</title>
    <%@include file="all_component/all_css.jsp"%>
    <style type="text/css">
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }.card:hover .card-body {
             background-color: #FcF7F7;
         }#toast {
              min-width: 300px;
              position: fixed;
              bottom: 30px;
              left: 50%;
              margin-left: -125px;
              background: #333;
              padding: 10px;
              color: white;
              text-align: center;
              z-index: 1;
              font-size: 18px;
              visibility: hidden;
              box-shadow: 0px 0px 100px #000;
          }

        #toast.display {
            visibility: visible;
            animation: fadeIn 0.5, fadeOut 0.5s 2.5s;
        }

        @keyframes fadeIn {from { bottom:0;
            opacity: 0;
        }

            to {
                bottom: 30px;
                opacity: 1;
            }

        }
        @keyframes fadeOut {form { bottom:30px;
            opacity: 1;
        }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

    </style>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>

<c:if test="${not empty addCart}">
    <div id="toast"> ${addCart} </div>

    <script type="text/javascript">
        showToast();
        function showToast(content)
        {
            $('#toast').addClass("display");
            $('#toast').html(content);
            setTimeout(()=>{
                $("#toast").removeClass("display");
            },2000)
        }
    </script>


</c:if>

<div class="container-fluid">
    <div class="row p-3">
        <%
            DeviceDAOImpl dao2=new DeviceDAOImpl(DBConnect.getConn());
            List<DeviceDetail> list2=dao2.getAllMobilePhone();
            for (DeviceDetail d:list2){
        %>
        <div class="col-md-3">
            <div class="card crd-ho mt-2">
                <div class="card-body text-center">
                    <img alt="" src="device/<%=d.getPhotoname()%>" style="width:200px; height:200px" class="img-thumblin">
                    <h6><%=d.getBrandname()%></h6>
                    <div class="row">
                        <a href="" class="btn btn-danger btn-sm ml-3">Add to Cart</a>
                        <a href="view_Device.jsp?did=<%=d.getDeviceid()%>"class="btn btn-primary btn-sm ml-1">View</a>
                        <a href="" class="btn btn-dark btn-sm ml-1">Rs.<%=d.getPrice()%></a>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
        </div>

    </div>

</div>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</body>
</html>
