<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<div class="container-fluid" style="height: 10px;background-color:#663399">

</div>
<div class="container-fluid p-3">
  <div class="row">
    <div class="col-md-8 text-warning">
      <h2><i class="fa-solid fa-shop"></i> Bumble-Bee</h2>
    </div>

    <c:if test="${not empty userobj}">
      <div class="col-md-4 text-right">
        <a href="checkout.jsp"><i class="fa-solid fa-cart-shopping fa-1x"></i></a>
        <a href="logging.jsp" class="btn btn-warning"><i class="fa-solid fa-user"></i> ${userobj.name}</a>
        <a href="logout" class="btn btn-dark"><i class="fa-solid fa-right-from-bracket"></i> Logout</a>
      </div>

    </c:if>

    <c:if test="${empty userobj}">
      <div class="col-md-4 text-right">
        <a href="register.jsp" class="btn btn-dark"><i class="fa-solid fa-registered"></i> Register</a>
        <a href="logging.jsp" class="btn btn-warning"><i class="fa-solid fa-right-to-bracket"></i> Logging</a>
      </div>

    </c:if>

  </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-custom">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="index.jsp"><i class="fa-solid fa-house"></i> Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="all_mobilephone.jsp"><i class="fa-solid fa-mobile"></i> Mobile Phones <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="all_laptop.jsp"><i class="fa-solid fa-laptop"></i> Laptops <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a href="ContactUs.jsp"><button class="btn btn-success my-2 my-sm-0" type="button"><i class="fa-sharp fa-solid fa-address-card"></i> Contact Us</button></a>
    </form>
  </div>
</nav>