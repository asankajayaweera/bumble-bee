<%@ page import="com.DAO.DeviceDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.DeviceDetail" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/11/2023
  Time: 2:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Admin: Edit Device</title>
    <%@include file="all_css.jsp"%>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="navigation_bar.jsp"%>
<c:if test="${empty userobj}">
    <c:redirect url="../logging.jsp"/>
</c:if>
        <div class="container">
<div class="row">
    <div class="col-md-4 offset-md-4">
        <div class="card ">
            <div class="card-body bg-success">
                <h4 class="text-center">Edit Device</h4>

                <%
                    int id =Integer.parseInt(request.getParameter("id"));
                    DeviceDAOImpl dao= new DeviceDAOImpl(DBConnect.getConn());
                    DeviceDetail d=dao.getDeviceById(id);
                %>
                <form action="../editDevice" method="post">
                    <input type="hidden" name="id" value="<%=d.getDeviceid()%>">
                    <div class="form-group col-md-4">
                        <select  placeholder="Select" name="type" value="<%=d.getDevicetype()%>">
                            <option>Mobile Phone</option>
                            <option>Laptop</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Enter Brand Name</label>
                        <input type="text" class="form-control" value="<%=d.getBrandname()%>" id="formGroupExampleInput" name="brandName" required="required" >
                    </div>
                    <div class="form-group">
                        <label>Enter Model Name</label>
                        <input type="text" class="form-control" value="<%=d.getModelname()%>"id="exampleInputPassword1"name="modelName" required="required">
                    </div>
                    <div class="form-group">
                        <label> Enter Price(Rs)</label>
                        <input type="number" class="form-control" value="<%=d.getPrice()%>" id="exampleInputModel"  name="price" required="required">
                    </div>
                    <div class="form-group">
                        <label>1st Installment(Rs)</label>
                        <input type="number" class="form-control" value="<%=d.getFirstinstallment()%>" id="exampleInput1stinstallment"  name="firstInstallment" >
                    </div>
                    <div class="form-group">
                        <label>2nd Installment(Rs)</label>
                        <input type="number" class="form-control"value="<%=d.getSecondinstallment()%>" id="exampleInput2ndinstallment"  name="secondInstallment" >
                    </div>
                    <div class="form-group">
                        <label>3rd Installment(Rs)</label>
                        <input type="number" class="form-control"  value="<%=d.getThirdinstallment()%>"id="exampleInput3rdinstallment" name="thirdInstallment">
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>


</div>

</div>
<%@include file="footer.jsp"%>
</body>
</html>
