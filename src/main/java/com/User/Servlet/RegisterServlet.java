package com.User.Servlet;

import com.DAO.UserDAOImpl;
import com.DB.DBConnect;
import com.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     try {
         String name=req.getParameter("name");
         String email=req.getParameter("email");
         String phonenu=req.getParameter("phonenu");
         String password=req.getParameter("password");
         String check=req.getParameter("check");

         //System.out.println(name+" "+email+" "+phonenu+" "+password+" "+check);
         User us=new User();
         us.setName(name);
         us.setEmail(email);
         us.setPhonenu(phonenu);
         us.setPassword(password);

         HttpSession session= req.getSession();

       if (check!=null)
       { UserDAOImpl dao=new UserDAOImpl(DBConnect.getConn());
           boolean f =dao.UserRegister(us);

           if(f)
           {
              // System.out.println("User Registered Successfully!!!");
               session.setAttribute("succMsg","User Registered Successfully!!!");
               resp.sendRedirect("register.jsp");
           }else {
               //System.out.println("Something Went Wrong!!!");
               session.setAttribute("failedMsg","Something Went Wrong!!!");
               resp.sendRedirect("register.jsp");
           }
       }else {
          // System.out.println("Please Check Term & Condition!!!");
           session.setAttribute("failedMsg","Please Check Term & Condition!!!");
           resp.sendRedirect("register.jsp");
       }
     } catch(Exception e){
         e.printStackTrace();
        }
    }
}
