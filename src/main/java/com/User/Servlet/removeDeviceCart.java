package com.User.Servlet;

import com.DAO.cartDAOImpl;
import com.DB.DBConnect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/remove_device")

public class removeDeviceCart extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int did=Integer.parseInt(req.getParameter("did"));
        cartDAOImpl dao=new cartDAOImpl(DBConnect.getConn());
        boolean f=dao.deleteDevice(did);
        HttpSession session= req.getSession();
        if(f)
        {
            session.setAttribute("succMsg","Device Remove From Cart!!!");
            resp.sendRedirect("checkout.jsp");
        }else
        {
            session.setAttribute("failedMsg","Something Wrong!!!");
            resp.sendRedirect("checkout.jsp");
        }
    }
}
