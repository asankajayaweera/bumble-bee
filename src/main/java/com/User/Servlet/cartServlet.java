package com.User.Servlet;

import com.DAO.DeviceDAOImpl;
import com.DAO.cartDAOImpl;
import com.DB.DBConnect;
import com.entity.DeviceDetail;
import com.entity.cart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.cert.Extension;

@WebServlet("/cart")

public class cartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {


            int did = Integer.parseInt(req.getParameter("did").trim());


            int uid = Integer.parseInt(req.getParameter("uid").trim());

            DeviceDAOImpl dao=new DeviceDAOImpl(DBConnect.getConn());
            DeviceDetail d=dao.getDeviceById(did);

            cart c = new cart();
            c.setDid(did);
            c.setUserid(uid);
            c.setBrandName(d.getBrandname());
            c.setModalName(d.getModelname());
            c.setPrice(Double.parseDouble(d.getPrice()));
            c.setTotalPrice(Double.parseDouble(d.getPrice()));

            cartDAOImpl dao2 = new cartDAOImpl(DBConnect.getConn());
            boolean f = dao2.addCart(c);
            HttpSession session=req.getSession();
            if (f)
            {
                session.setAttribute("addCart","Device Added to Cart");
                resp.sendRedirect("all_mobilephone.jsp");
            } else
            {
                session.setAttribute("Failed","Something Went Wrong");
                resp.sendRedirect("all_mobilephone.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
