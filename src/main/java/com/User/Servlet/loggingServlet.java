package com.User.Servlet;

import com.DAO.UserDAOImpl;
import com.DB.DBConnect;
import com.entity.User;
import com.mysql.cj.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/logging")
public class loggingServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UserDAOImpl dao=new UserDAOImpl(DBConnect.getConn());
            HttpSession session=req.getSession();
            String email=req.getParameter("email");
            String password=req.getParameter("password");

            if("asanka@gmail.com".equals(email) && "asanka".equals(password))
            {
                User us=new User();
                session.setAttribute("userobj",us);
                resp.sendRedirect("admin/home.jsp");
            }
            else
            {
                User us=dao.logging(email,password);
                if(us!=null)
                {
                    session.setAttribute("userobj", us);
                    resp.sendRedirect("index.jsp");

                }
                else
                {
                    session.setAttribute("failedMsg","Email Or Password Invalid");
                    resp.sendRedirect("logging.jsp");
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
