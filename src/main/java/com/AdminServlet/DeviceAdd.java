package com.AdminServlet;

import com.DAO.DeviceDAO;
import com.DAO.DeviceDAOImpl;
import com.DB.DBConnect;
import com.entity.DeviceDetail;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
@WebServlet("/add_device")
@MultipartConfig

public class DeviceAdd extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
try {
    String type=req.getParameter("type");
    String brand=req.getParameter("brandName");
    String model=req.getParameter("modelName");
    String price=req.getParameter("price");
    String firstInstallment=req.getParameter("firstInstallment");
    String secondInstallment=req.getParameter("secondInstallment");
    String thirdInstallment=req.getParameter("thirdInstallment");
    Part part= req.getPart("bimg");
    String fileName=part.getSubmittedFileName();

    DeviceDetail d=new DeviceDetail(type,brand,model,price,firstInstallment,secondInstallment,thirdInstallment,fileName);
    DeviceDAOImpl dao=new DeviceDAOImpl(DBConnect.getConn());

    boolean f= dao.addDevice(d);
    HttpSession session= req.getSession();
    if (f){
        String path=getServletContext().getRealPath("")+"device";

        File file = new File(path);

        part.write(path + File.separator +fileName);

        session.setAttribute("successMsg","Device Added Successfully!!!");
        resp.sendRedirect("admin/add_device.jsp");
    }else {
        session.setAttribute("failedMsg","Something Went Wrong!!!");
        resp.sendRedirect("admin/add_device.jsp");
    }
}catch(Exception e){
    e.printStackTrace();
}
    }
}
