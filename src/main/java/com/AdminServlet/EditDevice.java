package com.AdminServlet;

import com.DAO.DeviceDAOImpl;
import com.DB.DBConnect;
import com.entity.DeviceDetail;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/editDevice")
public class EditDevice extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id=Integer.parseInt(req.getParameter("id"));
            String type=req.getParameter("type");
            String brand=req.getParameter("brandName");
            String model=req.getParameter("modelName");
            String price=req.getParameter("price");
            String firstInstallment=req.getParameter("firstInstallment");
            String secondInstallment=req.getParameter("secondInstallment");
            String thirdInstallment=req.getParameter("thirdInstallment");

            DeviceDetail d=new DeviceDetail();
            d.setDeviceid(id);
            d.setDevicetype(type);
            d.setBrandname(brand);
            d.setModelname(model);
            d.setPrice(price);
            d.setFirstinstallment(firstInstallment);
            d.setSecondinstallment(secondInstallment);
            d.setThirdinstallment(thirdInstallment);

            DeviceDAOImpl dao=new DeviceDAOImpl(DBConnect.getConn());
            boolean f=dao.updateEditDevice(d);
            HttpSession session= req.getSession();

            if (f){
                session.setAttribute("succMsg","Device Updated Successfully!!!");
                resp.sendRedirect("admin/all_devices.jsp");
            }else{
                session.setAttribute("FailedMsg","Device Updated Not Success!!!");
                resp.sendRedirect("admin/all_devices.jsp");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
