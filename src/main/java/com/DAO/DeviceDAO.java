package com.DAO;

import com.entity.DeviceDetail;

import java.util.List;

public interface DeviceDAO {
    public boolean addDevice(DeviceDetail d);
    public List<DeviceDetail> getAllDevice();
    public DeviceDetail getDeviceById(int id);
    public boolean updateEditDevice(DeviceDetail d);
    public boolean deleteDevice(int id);
    public List<DeviceDetail> getNewDevice();
    public List<DeviceDetail> getLaptops();
    public List<DeviceDetail> getAllMobilePhone();
    public List<DeviceDetail> getAllLaptop();





}
