package com.DAO;

import com.entity.DeviceDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DeviceDAOImpl implements DeviceDAO {
    private Connection conn;

    public DeviceDAOImpl(Connection conn) {
        super();
        this.conn = conn;
    }


    public boolean addDevice(DeviceDetail d) {
        boolean f = false;
        try {
            String sql = "insert into device_details(devicetype,brandname,model,price,1stinstallment,2ndinstallment,3rdinstallment,photo) values(?,?,?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, d.getDevicetype());
            ps.setString(2, d.getBrandname());
            ps.setString(3, d.getModelname());
            ps.setString(4, d.getPrice());
            ps.setString(5, d.getFirstinstallment());
            ps.setString(6, d.getSecondinstallment());
            ps.setString(7, d.getThirdinstallment());
            ps.setString(8, d.getPhotoname());

            int i = ps.executeUpdate();
            if (i == 1) {
                f = true;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return f;
    }

    public List<DeviceDetail> getAllDevice() {


        List<DeviceDetail> list = new ArrayList<DeviceDetail>();
        DeviceDetail d = null;

        try {

            String sql = "SELECT * FROM device_details";
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                d = new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));
                list.add(d);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;


    }

    @Override
    public DeviceDetail getDeviceById(int id) {
        DeviceDetail d=null;
        try {
            String sql="Select * from device_details where deviceid=?";
            PreparedStatement ps= conn.prepareStatement(sql);
            ps.setInt(1,id);

            ResultSet rs= ps.executeQuery();
            while(rs.next())
            {
                d=new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));

            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return d;
    }

    @Override
    public boolean updateEditDevice(DeviceDetail d) {
        boolean f=false;
        try {
            String sql="update device_details set devicetype=?,brandname=?,model=?,price=?,1stinstallment=?,2ndinstallment=?,3rdinstallment=? where deviceId=?";
            PreparedStatement ps= conn.prepareStatement(sql);
            ps.setString(1,d.getDevicetype());
            ps.setString(2,d.getBrandname());
            ps.setString(3,d.getModelname());
            ps.setString(4,d.getPrice());
            ps.setString(5,d.getFirstinstallment());
            ps.setString(6,d.getSecondinstallment());
            ps.setString(7,d.getThirdinstallment());
            ps.setInt(8,d.getDeviceid());

            int i=ps.executeUpdate();
            if (i==1){
                f=true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        return f;
    }

    @Override
    public boolean deleteDevice(int id) {
        boolean f = false;
        try {
            String sql = "delete  from device_details where deviceId=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            int i = ps.executeUpdate();
            if (i == 1) {
                f = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
       }
    public List<DeviceDetail> getNewDevice() {

        List<DeviceDetail> list=new ArrayList<DeviceDetail>();
        DeviceDetail d=null;
        try {
            String sql="select * from device_details where deviceType=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,"Mobile Phone");
            ResultSet rs=ps.executeQuery();
            int i=1;
            while(rs.next() && i<=4)
            {
                d=new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));
                list.add(d);
                i++;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<DeviceDetail> getLaptops() {
        List<DeviceDetail> list=new ArrayList<DeviceDetail>();
        DeviceDetail d=null;
        try {
            String sql="select * from device_details where deviceType=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,"Laptop");
            ResultSet rs=ps.executeQuery();
            int i=1;
            while(rs.next() && i<=4)
            {
                d=new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));
                list.add(d);
                i++;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<DeviceDetail> getAllMobilePhone() {
        List<DeviceDetail> list=new ArrayList<DeviceDetail>();
        DeviceDetail d=null;
        try {
            String sql="select * from device_details where deviceType=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,"Mobile Phone");
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                d=new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));
                list.add(d);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<DeviceDetail> getAllLaptop() {
        List<DeviceDetail> list=new ArrayList<DeviceDetail>();
        DeviceDetail d=null;
        try {
            String sql = "select * from device_details where deviceType=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "Laptop");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                d = new DeviceDetail();
                d.setDeviceid(rs.getInt(1));
                d.setDevicetype(rs.getString(2));
                d.setBrandname(rs.getString(3));
                d.setModelname(rs.getString(4));
                d.setPrice(rs.getString(5));
                d.setFirstinstallment(rs.getString(6));
                d.setSecondinstallment(rs.getString(7));
                d.setThirdinstallment(rs.getString(8));
                d.setPhotoname(rs.getString(9));
                list.add(d);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
}