package com.DAO;

import com.entity.DeviceDetail;
import com.entity.cart;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class cartDAOImpl implements cartDAO {
    private Connection conn;
    public cartDAOImpl(Connection conn){
        this.conn=conn;
    }
    @Override
    public boolean addCart(cart c) {
        boolean f=false;
        try {
            String sql="insert into cart(did,uid,brandName,modalName,price,total_price) values(?,?,?,?,?,?)";
            PreparedStatement ps= conn.prepareStatement(sql);
            ps.setInt(1,c.getDid());
            ps.setInt(2,c.getUserid());
            ps.setString(3,c.getBrandName());
            ps.setString(4,c.getModalName());
            ps.setDouble(5,c.getPrice());
            ps.setDouble(6,c.getTotalPrice());
            int i=ps.executeUpdate();
            if(i==1){
                f=true;
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        return f;
    }

    @Override
    public List<cart> getDeviceByUser(int uerId) {
        List<cart> list=new ArrayList<cart>();
        cart c=null;
        double totalPrice=0;
        try {
            String sql="select * from cart where uid=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1,uerId);

            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                c=new cart();
                c.setCid(rs.getInt(1));
                c.setDid(rs.getInt(2));
                c.setUserid(rs.getInt(3));
                c.setBrandName(rs.getString(4));
                c.setModalName(rs.getString(5));
                c.setPrice(rs.getDouble(6));

                totalPrice =totalPrice+rs.getDouble(7);
                c.setTotalPrice(totalPrice);

                list.add(c);

            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean deleteDevice(int did) {
        boolean f=false;
        try {
            String sql="delete from cart where did=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1,did);
              int i=ps.executeUpdate();
            if(i==1)
            {
                f=true;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return f;
    }
}
