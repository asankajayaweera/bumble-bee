package com.DAO;

import com.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAOImpl implements UserDAO{
    private Connection conn;
    public UserDAOImpl(Connection conn){
        super();
        this.conn=conn;
    }

    @Override
    public boolean UserRegister(User us) {
        boolean f=false;
        try {
            String sql="Insert Into User(name,email,phonenu,password) values(?,?,?,?) ";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1, us.getName());
            ps.setString(2, us.getEmail());
            ps.setString(3, us.getPhonenu());
            ps.setString(4, us.getPassword());

            int i=ps.executeUpdate();
            if(i==1);{
                f=true;
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return f;
    }


    public User logging(String email, String password) {
        User us=null;
        try {
            String sql="select * from user where email=? and password=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,email);
            ps.setString(2,password);

            ResultSet rs= ps.executeQuery();
            while (rs.next()){
                us=new User();
                us.setId(rs.getInt(1));
                us.setName(rs.getString(2));
                us.setEmail(rs.getString(4));
                us.setPhonenu(rs.getString(3));
                us.setPassword(rs.getString(5));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return us;
        }
    }
