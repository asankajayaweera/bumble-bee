package com.entity;

public class DeviceDetail {
    private int deviceid;
    private String devicetype;
    private String brandname;
    private String modelname;
    private String price;
    private String firstinstallment;
    private String secondinstallment;
    private String thirdinstallment;
    private String photoname;
    public DeviceDetail(){
        super();
    }

    public DeviceDetail(String devicetype, String brandname, String modelname, String price, String firstinstallment, String secondinstallment, String thirdinstallment, String photoname) {
        super();
        this.devicetype = devicetype;
        this.brandname = brandname;
        this.modelname = modelname;
        this.price = price;
        this.firstinstallment = firstinstallment;
        this.secondinstallment = secondinstallment;
        this.thirdinstallment = thirdinstallment;
        this.photoname = photoname;
    }

    public int getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(int deviceid) {
        this.deviceid = deviceid;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getModelname() {
        return modelname;
    }

    public void setModelname(String modelname) {
        this.modelname = modelname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFirstinstallment() {
        return firstinstallment;
    }

    public void setFirstinstallment(String firstinstallment) {
        this.firstinstallment = firstinstallment;
    }

    public String getSecondinstallment() {
        return secondinstallment;
    }

    public void setSecondinstallment(String secondinstallment) {
        this.secondinstallment = secondinstallment;
    }

    public String getThirdinstallment() {
        return thirdinstallment;
    }

    public void setThirdinstallment(String thirdinstallment) {
        this.thirdinstallment = thirdinstallment;
    }

    public String getPhotoname() {
        return photoname;
    }

    public void setPhotoname(String photoname) {
        this.photoname = photoname;
    }

    @Override
    public String toString() {
        return "DeviceDetail{" +
                "deviceid=" + deviceid +
                ", devicetype='" + devicetype + '\'' +
                ", brandname='" + brandname + '\'' +
                ", modelname='" + modelname + '\'' +
                ", price=" + price +
                ", firststinstallment=" + firstinstallment +
                ", secondinstallment=" + secondinstallment +
                ", thirdinstallment=" + thirdinstallment +
                ", photoname='" + photoname + '\'' +
                '}';
    }
}
