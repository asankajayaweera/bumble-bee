<%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 4/3/2023
  Time: 8:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Contact Us</title>
    <%@include file="all_component/all_css.jsp"%>
    <style>
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }
        .card:hover .card-body {
            background-color: #FcF7F7;
        }
        body {
            background-image: url('image/1.jpg');
            background-size: cover;
            filter: randomBars(2px);
        }
    </style>
</head>
<body>
<%@include file="all_component/navigation_bar.jsp"%>

<div class="col-md-12">
    <div class="card mt-2">
        <div class="card-body">
            <h3 class="text-center text-warning">Contact Us</h3>
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <i class="fa-solid fa-phone fa-3x"></i><h4 class="text-center">Phone Number : 077-8172818</h4>
                        <h4 class="text-center">Email : hmasankajj@gmail.com</h4>
                    </div>
            </form>
        </div>
    </div>

</div>
</div>


<%@include file="all_component/footer.jsp"%>
</body>
</html>
