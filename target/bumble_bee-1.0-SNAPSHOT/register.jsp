<%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/2/2023
  Time: 9:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
  <title>Register</title>
  <%@include file="all_component/all_css.jsp"%>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>
<div class="container p-2">
  <div class="row">
    <div class="col-md-4 offset-md-4">
      <div class="card bg-success">
        <div class="card-body">
          <h4 class="text-center">Registration Page</h4>

          <c:if test="${not empty succMsg}">
          <p class="text-center text-warning">${succMsg}</p>
            <c:remove var="succMsg" scope="session"/>
          </c:if>
          <c:if test="${not empty failedMsg}">
            <p class="text-center text-danger">${failedMsg}</p>
            <c:remove var="failedMsg" scope="session"/>
          </c:if>

          <form action="register" method="post">
            <div class="form-group">
              <label for="exampleInputEmail1">Enter  Name</label>
              <input type="text" class="form-control" id="exampleFirstName" aria-describedby="emailHelp" required="required" name="name" >
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="required" name="email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Phone Number</label>
              <input type="number" class="form-control" id="exampleInputNumber" aria-describedby="emailHelp" required="required" name="phonenu">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Password</label>
              <input type="password" class="form-control" id="" aria-describedby="emailHelp" required="required" name="password" >
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1"name="check">
              <label class="form-check-label" for="exampleCheck1">Agree Term & Condition</label>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</html>
