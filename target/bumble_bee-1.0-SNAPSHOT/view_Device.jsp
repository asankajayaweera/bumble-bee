<%@ page import="com.DAO.DeviceDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.DeviceDetail" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/29/2023
  Time: 11:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Device</title>
  <%@include file="all_component/all_css.jsp"%>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>
<%
  int did=Integer.parseInt(request.getParameter("did"));
  DeviceDAOImpl dao=new DeviceDAOImpl(DBConnect.getConn());
  DeviceDetail d=dao.getDeviceById(did);
%>
<div class="container p-3">
  <div class="row">
    <div class="col-md-6 text-center p-5 border bg-white">
      <img src="device/<%=d.getPhotoname()%>" style="height: 150px; width: 150px"><br>
      <h4 class="mt-3">Brand Name: <span class="text-success"><%=d.getBrandname()%></span></h4>
      <h4>Modal: <span class="text-warning "><%=d.getModelname()%></span></h4>
      <h4>Price(Rs): <span class="text-danger"><%=d.getPrice()%> </span></h4>
    </div>
    <div class="col-md-6 text-center p-5 border bg-white">
      <h1><%=d.getBrandname()%></h1>
      <div class="row">
        <div class="col-md-4 text-center p-2 text-danger"><i class="fa-solid fa-money-bill fa-3x"></i><p>Cash On delivery</p></div>
        <div class="col-md-4 text-center p-2 text-danger"><i class="fa-solid fa-rotate-left fa-3x"></i><p>Return</p></div>
        <div class="col-md-4 text-center p-2 text-warning"><i class="fa-solid fa-truck fa-3x"></i><p>Free Shipping</p></div>
      </div>
        <div class=" p-3">
          <a href="" class="btn btn-primary "><i class="fa-solid fa-cart-shopping"></i> Add to cart</a>
          <a href="" class="btn btn-danger "><i class="fa-solid fa-rupee-sign"></i> <%=d.getPrice()%></a><br><br>
          <a href="" class="btn btn-success">Firts Installment(Rs): <%=d.getFirstinstallment()%></a><br><br>
          <a href="" class="btn btn-success">Second Installment(Rs): <%=d.getSecondinstallment()%></a><br><br>
          <a href="" class="btn btn-success">Third Installment(Rs): <%=d.getThirdinstallment()%></a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</html>
