<%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/2/2023
  Time: 10:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
  <title>Title</title>
  <%@include file="all_component/all_css.jsp"%>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="all_component/navigation_bar.jsp"%>
<div class="container p-2">
  <div class="row">
    <div class="col-md-4 offset-md-4">
      <div class="card bg-success">
        <div class="card-body">
          <h4 class="text-center">Logging Page</h4>
          <c:if test="${not empty failedMsg}">
            <h5 class="text-center text-danger">${failedMsg}</h5>
            <c:remove var="failedMsg" scope="session"/>
          </c:if>

          <c:if test="${not empty succMsg}">
            <h5 class="text-center text-warning">${succMsg}</h5>
            <c:remove var="succMsg" scope="session"/>
          </c:if>

          <form action="logging" method="post">
            <div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email Address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="required" name="email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Password</label>
                <input type="password" class="form-control" id="" aria-describedby="emailHelp" required="required" name="password" >
                <div class="text-center mt-3" >
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <br><br>
                  <a href="register.jsp">Create a Account</a>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div style="margin-top:50px ">
  <%@include file="all_component/footer.jsp"%></div>
</body>
</html>
