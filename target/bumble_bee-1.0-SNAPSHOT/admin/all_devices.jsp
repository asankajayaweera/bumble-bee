<%@ page import="com.DAO.DeviceDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.DeviceDetail" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/11/2023
  Time: 2:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<head>
    <title>Admin: All Device</title>
    <%@include file="all_css.jsp"%>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="navigation_bar.jsp"%>
<c:if test="${empty userobj}">
    <c:redirect url="../logging.jsp"/>
</c:if>
        <h3 class="text-center">Hello Admin</h3>

<c:if test="${not empty succMsg}">
    <h5 class="text-center text-primary">${succMsg}</h5>
    <c:remove var="succMsg" scope="session"/>
</c:if>
<c:if test="${not empty failedMsg}">
    <h5 class="text-center text-warning">${failedMsg}</h5>
    <c:remove var="failedMsg" scope="session"/>
</c:if>


<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Image</th>
        <th scope="col">Device</th>
        <th scope="col">Brand Name</th>
        <th scope="col">Model Name</th>
        <th scope="col">Price</th>
        <th scope="col">1st Installment</th>
        <th scope="col">2nd Installment</th>
        <th scope="col">3rd installment</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <%DeviceDAOImpl dao= new DeviceDAOImpl(DBConnect.getConn());
        List<DeviceDetail> list= dao.getAllDevice();
        for (DeviceDetail d: list){
            %>
    <tr>
        <td><%=d.getDeviceid()%></td>
        <td><img src="../device/<%=d.getPhotoname()%>" style="width:50px; height: 50px;"></td>
        <td><%=d.getDevicetype()%></td>
        <td><%=d.getBrandname()%></td>
        <td><%=d.getModelname()%></td>
        <td><%=d.getPrice()%></td>
        <td><%=d.getFirstinstallment()%></td>
        <td><%=d.getSecondinstallment()%></td>
        <td><%=d.getThirdinstallment()%></td>
        <td>
            <a href="edit_device.jsp?id=<%=d.getDeviceid()%>" class="btn btn-sm btn-primary"><i class="fa-solid fa-pen-to-square"></i> Edit</a>
            <a href="../delete?id=<%=d.getDeviceid()%>" class="btn btn-sm btn-danger"><i class="fa-solid fa-trash"></i> Delete</a>
        </td>
    </tr>
    <%
        }
        %>

    </tbody>
</table>
<div style="margin-top:100px;">
    <%@include file="footer.jsp"%>
</div>
</body>

</html>