<div class="container-fluid" style="height: 10px;background-color:#663399">

</div>
<div class="container-fluid p-3">
  <div class="row">
    <div class="col-md-8 text-warning">
      <h2><i class="fa-solid fa-shop"></i> Bumble-Bee</h2>
    </div>

    <div class="col-md-4 text-right">
      <a data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-warning"><i class="fa-solid fa-right-to-bracket"></i> Logout</a>
    </div>
  </div>

</div>
<!--logout modal-->
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center">
        <h4>Do You want to Logout</h4>
          <br>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <a href="../logout" type="button" class="btn btn-danger">Logout</a>
      </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
<!--logout modal end-->
<nav class="navbar navbar-expand-lg navbar-dark bg-custom">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="home.jsp"><i class="fa-solid fa-house"></i> Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
</nav>