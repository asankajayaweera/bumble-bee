<%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/10/2023
  Time: 10:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Admin:Home</title>
    <%@include file="all_css.jsp"%>
    <style type="text/css">
        a:hover{text-decoration: none; color:black;}
        a{text-decoration: none;color:black;}
    </style>
</head>
<body style="background-color: #f0f1f2;">
<%@include file="navigation_bar.jsp"%>
        <c:if test="${empty userobj}">
            <c:redirect url="../logging.jsp"/>
        </c:if>

<div class="container text-center">
    <h3>Welcome Admin</h3><br>
    <div class="row p-3 ">
        <div class="col-md-3">
            <a href="add_device.jsp">
                <div class="card">
                <div class="card-body text-center">
                    <i class="fa-solid fa-plus fa-3x text-success"></i><br>
                    <h3>Add Device</h3>
                </div>
            </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="all_devices.jsp">
                <div class="card">
                <div class="card-body text-center">
                    <i class="fa-solid fa-store fa-3x text-primary"></i><br>
                    <h3>All devices</h3>
                </div>
            </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="orders.jsp">
            <div class="card">
                <div class="card-body text-center">
                    <i class="fa-brands fa-first-order fa-3x text-warning"></i><br>
                   <h3>Orders</h3>
                </div>
            </div>
            </a>
        </div>

        <div class="col-md-3">
            <a data-toggle="modal" data-target= "#exampleModalCenter">
                <div class="card">
                    <div class="card-body text-center">
                        <i class="fa-solid fa-right-from-bracket text-danger fa-3x"></i><br>
                        <h3>Logout</h3>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<!--logout modal-->
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h4>Do You want to Logout</h4>
                    <br>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <a href="../logout" type="button" class="btn btn-danger">Logout</a>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!--logout modal end-->
<div style="margin-top:200px;">
<%@include file="footer.jsp"%>
</div>
</body>
</html>
