<%@ page import="com.DAO.DeviceDAOImpl" %>
<%@ page import="com.DB.DBConnect" %>
<%@ page import="com.entity.DeviceDetail" %>
<%@ page import="java.util.List" %>
<%@ page import="com.entity.User" %><%--
  Created by IntelliJ IDEA.
  User: Asanka
  Date: 3/29/2023
  Time: 9:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
  <title>Index</title>
  <%@include file="all_component/all_css.jsp"%>
  <style>
    .card:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .card:hover .card-body {
      background-color: #FcF7F7;
    }
    body {
      background-image: url('image/1.jpg');
      background-size: cover;
      filter: randomBars(2px);
    }
  </style>
</head>
<body style="background-color: #f0f1f2;">
<%User u=(User)session.getAttribute("userobj");%>
<%@include file="all_component/navigation_bar.jsp"%>
<div class="container-fluid back-img">
  <h3 class=" text-warning">Buy Best Products..</h3>
</div>
<div class="container text-center">
  <h3 class="text-Warning">Mobile Phones</h3>
  <div class="row">

      <%
        DeviceDAOImpl dao=new DeviceDAOImpl(DBConnect.getConn());
        List<DeviceDetail> list=dao.getNewDevice();
        for (DeviceDetail d:list){
      %>
    <div class="col-md-3">
      <div class="card">
        <div class="card-body text-center">
          <img alt="" src="device/<%=d.getPhotoname()%>" style="width:200px; height:200px" class="img-thumblin">
          <h6><%=d.getBrandname()%></h6>
          <div class="row">
            <%
              if (u==null){%>
                <a href="logging.jsp  " class="btn btn-danger btn-sm ml-3">Add to Cart</a>
            <%
            }else{
            %>
            <a href="cart?did=<%=d.getDeviceid()%> &&uid=<%=u.getId()%>" class="btn btn-danger btn-sm ml-3">Add to Cart</a>
            <%
              }
            %>

            <a href="view_Device.jsp?did=<%=d.getDeviceid()%>" class="btn btn-primary btn-sm ml-1">View</a>
            <a href="" class="btn btn-dark btn-sm ml-1">Rs.<%=d.getPrice()%></a>
          </div>
        </div>
      </div>
    </div>
      <%
        }
      %>


    </div>
  </div>
</div>

</div>
<div class="container text-center">
  <h3 class="text-warning">Laptops</h3>
  <div class="row">
  <%
    DeviceDAOImpl dao1=new DeviceDAOImpl(DBConnect.getConn());
    List<DeviceDetail> list1=dao1.getLaptops();
    for (DeviceDetail d:list1){
  %>
    <div class="col-md-3">
      <div class="card">
        <div class="card-body text-center">
          <img alt="" src="device/<%=d.getPhotoname()%>" style="width:200px; height:200px" class="img-thumblin">
          <h6><%=d.getBrandname()%></h6>
          <div class="row">
            <%
              if (u==null){%>
            <a href="logging.jsp  " class="btn btn-danger btn-sm ml-3">Add to Cart</a>
            <%
            }else{
            %>
            <a href="cart?did=<%=d.getDeviceid()%> &&uid=<%=u.getId()%>" class="btn btn-danger btn-sm ml-3">Add to Cart</a>
            <%
              }
            %>
            <a href="view_Device.jsp?did=<%=d.getDeviceid()%>" class="btn btn-primary btn-sm ml-1">View</a>
            <a href="" class="btn btn-dark btn-sm ml-1">Rs.<%=d.getPrice()%></a>
          </div>
        </div>
      </div>
    </div>
  <%}
    %>
  </div>
</div>
</div>
<%@include file="all_component/footer.jsp"%>
</body>
</html>
